Postgresql
##########

.. contents::
  :backlinks: top

Full text search
****************

*(...) full-text search refers to techniques for searching a single computer-stored document or a collection in a full
text database; (...) distinguished from searches based on metadata or on parts of the original texts represented in
databases (such as titles, abstracts, selected sections, or bibliographical references). Wikipedia*

Like Operator
=============

.. code-block:: sql

   SELECT * FROM table_name
   WHERE
      column_name LIKE '%fox%' AND
      column_name LIKE '%dog%';
   # va tester pour chaque colonne si il y a un fox ou un dog dans une phrase.

Inconvénients : si  on cherche par exemple le mot "query" et qu'il y a "queries", la requête ne vas pous nous
retourner la ligne. Mais aussi si on recherche par rapport à "fox" et qu'il ya foxcity dans la db, foxcity sera
retourné.

tsvector Function
=================

.. code-block:: sql

   SELECT to_tsvector('The quick brown fox jumped over the lazy dog.');

   to_tsvector result
   -------------------------------------------------------
   'brown':3 'dog':9 'fox':4 'jump':5 'lazi':8 'quick':2

tsvector permet de retourner un vecteur avec " mot : sa_position" et va ommettre tous les mots qui n'ont pas vraiment
de sens dans la phrase comme les articles définies/indéfinies/partitif (le, la , un , des, du...)
ou des conjonctions (mais ou et or ni car)

On peut spécifier la langue :

.. code-block:: sql

   SELECT to_tsvector('Portuguese', 'Zebras caolhas de Java querem mandar fax para gigante em New York');

   to_tsvector result
   ------------------------------------------------------------------------------------
   'caolh':2 'fax':7 'gigant':9 'jav':4 'mand':6 'new':11 'quer':5 'york':12 'zebr':1


C'est intéressant d'utiliser cette fonction dans certain langage comme le portugais ou le français,
car par exemple gigante peut prendre plusieur forme : gigante pour masculin, giganta pour féminin,
gigantes pour masculin/pluriel et gigantas pour féminin/pluriel. Avec tsvector peu importe si c'est féminin
ou masculin ou pluriel, la fonction nous retournera la ligne. *(NLP)*

tsquery Function
================

Ca permet d'enlever aussi le pluriel, masculin, féminin, les conjuguaisons, les articles/conjonctions...
Pour en savoir plus go `ici`_.

.. _ici: https://docs.postgresql.fr/9.2/textsearch-controls.html


Nous allons utiliser l'opérateur '@@' pour vérifier si le(s) mot(s) de tsquery matchent avec le vecteur de
tsvector. Essayons avec "foxes":

.. code-block:: sql

   SELECT to_tsvector('The quick brown fox jumped over the lazy dog')
          @@ to_tsquery('foxes');

   ?column?
   ----------
   t

.. code-block:: sql

   SELECT to_tsvector('The quick brown fox jumped over the lazy dog')
          @@ to_tsquery('foxtrot');

   ?column?
   -----------
   f

Si on lui demande "foxtrot", notre db va lui retourner false car ca ne va pas correspondre au mot "fox"
**Contrairement à l'opérateur LIKE**

On peut jouer avec les opérateurs et/ou.

.. code-block:: sql

   SELECT to_tsvector('The quick brown fox jumped over the lazy dog')
          @@ to_tsquery('fox & dog');

   ?column?
   ----------
   t

.. code-block:: sql

    SELECT to_tsvector('The quick brown fox jumped over the lazy dog')
        @@ to_tsquery('fox | clown');

    ?column?
    -----------
    t

On peut stocker dans une colonne la traduction du document_text en tsvector (une colonne en plus --> plus de donnée
à scanner lors des lectures = temps de réponse plus long )
Ou on peut faire la traduction à la volé (uniquement à la lecture):

.. code-block:: sql

   SELECT document_id, document_text FROM documents
   WHERE to_tsvector(document_text) @@ to_tsquery('jump & quick');

* **pros :**
    Pas besoin de stocker une colonne en plus pour traduire le texte en tsvector

* **cons :**
    pour chaque lecture il va falloir effectuer l'action tsvector sur le document_text ce qui peut être couteux en cpu
    et en performance.

Full text search Dictionnaries
==============================
TODO

Connect postgres to elasticsearch with abc
******************************************

lire cet `article`_.

.. _article: https://medium.appbase.io/cli-for-indexing-data-from-postgres-to-elasticsearch-6eebc5cc0f0f


Bibliography
************

* [1] "Mastering PostgreSQL Tools: Full-Text Search and Phrase Search",  https://www.compose.com/articles/mastering-postgresql-tools-full-text-search-and-phrase-search/

