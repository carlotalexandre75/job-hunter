Welcome to the jungle
#####################

Api access
**********

.. code-block:: bash

   $ curl 'https://csekhvms53-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20vanilla%20JavaScript%20(lite)%203.30.0%3Breact-instantsearch%205.3.2%3BJS%20Helper%202.26.1&x-algolia-application-id=CSEKHVMS53&x-algolia-api-key=02f0d440abc99cae37e126886438b266' \
   -H 'accept: application/json' \
   -H 'Referer: https://www.welcometothejungle.co/jobs?query=deg&page=1&configure%5Bfilters%5D=website.reference%3Awttj_fr&configure%5BhitsPerPage%5D=30' \
   -H 'Origin: https://www.welcometothejungle.co' \
   -H 'content-type: application/x-www-form-urlencoded' \
   --data '{"requests":[{"indexName":"wk_cms_jobs_production","params":"query=devops"}]}' \
   --compressed | jq '.results[0].hits'



.. code-block:: json

  {
    "slug": "devops_paris",
    "name": "DevOps",
    "profile": "**Technical Skill**\r\n* Maîtrise de Docker / Kubernetes (Helm Chart);\r\n* Connaissance des solutions Cloud (de préférence Azure, formation possible);\r\n* Ansible / Terraform;\r\n* RunDeck;\r\n* GitlabCI;\r\n* Redis;\r\n* ElasticSearch;\r\n* PHP 7/ Symfony 3 & 4.\r\n\r\n**Competencies**\r\n* Référent technique;\r\n* Solutionneur;\r\n* Sensible aux problématiques de sécurité;\r\n* Accorde de la valeur au performances de la solution.\r\n",
    "published_at": "2019-02-19T17:41:36.737+01:00",
    "website": {
      "reference": "wttj_fr"
    },
    "organization": {
      "name": "Wynd",
      "slug": "wynd",
      "reference": "PzGb5KW",
      "size": "Entre 250 et 2000 salariés",
      "logo": {
        "url": "https://cdn.welcometothejungle.co/uploads/organization/logo/1538/154238/wynd.png",
        "thumb": {
          "url": "https://cdn.welcometothejungle.co/uploads/organization/logo/1538/154238/thumb_wynd.png"
        }
      },
      "cover_image": {
        "url": "https://cdn.welcometothejungle.co/uploads/website_organization/cover_image/wttj_fr/wynd.jpg",
        "large": {
          "url": "https://cdn.welcometothejungle.co/uploads/website_organization/cover_image/wttj_fr/large_wynd.jpg"
        },
        "small": {
          "url": "https://cdn.welcometothejungle.co/uploads/website_organization/cover_image/wttj_fr/small_wynd.jpg"
        },
        "thumb": {
          "url": "https://cdn.welcometothejungle.co/uploads/website_organization/cover_image/wttj_fr/thumb_wynd.jpg"
        }
      },
      "website_organization": {
        "slug": "wynd"
      }
    },
    "new_sectors": [
      {
        "name": "Logiciels",
        "parent": "Tech"
      },
      {
        "name": "SaaS / Cloud Services",
        "parent": "Tech"
      },
      {
        "name": "E-commerce",
        "parent": "Distribution"
      }
    ],
    "new_sectors_name": [
      {
        "Tech": "Logiciels"
      },
      {
        "Tech": "SaaS / Cloud Services"
      },
      {
        "Distribution": "E-commerce"
      }
    ],
    "contract_type": "FULL_TIME",
    "contract_type_names": {
      "fr": "CDI",
      "en": "Full-Time"
    },
    "experience_level_minimum": 4,
    "office": {
      "city": "Paris",
      "country": "France",
      "district": "Paris"
    },
    "department": "Tech",
    "profession": {
      "name": "DevOps / Infra",
      "category": "Tech"
    },
    "profession_name": {
      "Tech": "DevOps / Infra"
    },
    "is_remote": true,
    "_geoloc": {
      "lat": 48.8468279,
      "lng": 2.279092
    },
    "objectID": "160371",
    "_highlightResult": {
      "name": {
        "value": "<em>DevOps</em>",
        "matchLevel": "full",
        "fullyHighlighted": true,
        "matchedWords": [
          "devops"
        ]
      },
      "profile": {
        "value": "**Technical Skill**\r\n* Maîtrise de Docker / Kubernetes (Helm Chart);\r\n* Connaissance des solutions Cloud (de préférence Azure, formation possible);\r\n* Ansible / Terraform;\r\n* RunDeck;\r\n* GitlabCI;\r\n* Redis;\r\n* ElasticSearch;\r\n* PHP 7/ Symfony 3 & 4.\r\n\r\n**Competencies**\r\n* Référent technique;\r\n* Solutionneur;\r\n* Sensible aux problématiques de sécurité;\r\n* Accorde de la valeur au performances de la solution.\r\n",
        "matchLevel": "none",
        "matchedWords": []
      },
      "organization": {
        "name": {
          "value": "Wynd",
          "matchLevel": "none",
          "matchedWords": []
        }
      },
      "new_sectors": [
        {
          "name": {
            "value": "Logiciels",
            "matchLevel": "none",
            "matchedWords": []
          },
          "parent": {
            "value": "Tech",
            "matchLevel": "none",
            "matchedWords": []
          }
        },
        {
          "name": {
            "value": "SaaS / Cloud Services",
            "matchLevel": "none",
            "matchedWords": []
          },
          "parent": {
            "value": "Tech",
            "matchLevel": "none",
            "matchedWords": []
          }
        },
        {
          "name": {
            "value": "E-commerce",
            "matchLevel": "none",
            "matchedWords": []
          },
          "parent": {
            "value": "Distribution",
            "matchLevel": "none",
            "matchedWords": []
          }
        }
      ],
      "new_sectors_name": [
        {
          "Tech": {
            "value": "Logiciels",
            "matchLevel": "none",
            "matchedWords": []
          }
        },
        {
          "Tech": {
            "value": "SaaS / Cloud Services",
            "matchLevel": "none",
            "matchedWords": []
          }
        },
        {
          "Distribution": {
            "value": "E-commerce",
            "matchLevel": "none",
            "matchedWords": []
          }
        }
      ],
      "contract_type_names": {
        "fr": {
          "value": "CDI",
          "matchLevel": "none",
          "matchedWords": []
        },
        "en": {
          "value": "Full-Time",
          "matchLevel": "none",
          "matchedWords": []
        }
      },
      "office": {
        "city": {
          "value": "Paris",
          "matchLevel": "none",
          "matchedWords": []
        },
        "country": {
          "value": "France",
          "matchLevel": "none",
          "matchedWords": []
        },
        "district": {
          "value": "Paris",
          "matchLevel": "none",
          "matchedWords": []
        }
      },
      "department": {
        "value": "Tech",
        "matchLevel": "none",
        "matchedWords": []
      },
      "profession": {
        "name": {
          "value": "<em>DevOps</em> / Infra",
          "matchLevel": "full",
          "fullyHighlighted": false,
          "matchedWords": [
            "devops"
          ]
        },
        "category": {
          "value": "Tech",
          "matchLevel": "none",
          "matchedWords": []
        }
      }
    }
  }

