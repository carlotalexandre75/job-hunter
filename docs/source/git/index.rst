GIT
###

.. contents::
  :backlinks: top

Installation
************

Pour installer git click  `ici`_.

.. _ici: https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git


Overview
********


.. figure:: ../_static/git/git-schema.png
  :align: center



Configurer son git
******************

On peut configurer notre git en global ou en local (propre à un seul projet)
Pour le configurer en global :

.. code-block:: bash

   $ git config --global user.name "your_username :)"
   $ git config --global user.email "your email same of your account git "

Télécharger un repo distant
***************************

On peut tirer un repo distant via la commande clone :

.. code-block:: bash

   $ git clone git@gitlab.com:lcaminale/jobs-hunter-esme.git # via ssh
   $ git clone https://gitlab.com/lcaminale/jobs-hunter-esme.git # via https

Créer un repo avec un projet existant en local
*****************************************************

.. code-block:: bash

   $ cd existing_folder
   $ git init
   $ git remote add origin git@gitlab.com:lcaminale/jobs-hunter-esme.git # link my local dir to a distant
   $ git add . # add all files
   $ git commit -m "Initial commit"
   $ git push -u origin master


Ajouter & Valider
*****************

Vous pouvez proposer un changement en exécutant les commandes :

.. code-block:: bash

   $ git add <filename> # add file by file
   $ git add . # add all files/directories

Pour valider ces changements, utilisez :

.. code-block:: bash

   $ git commit -m "Message de validation"

*Il existe pleins d'autres variantes avec git commit -m -A -a...*

Le fichier est donc ajouté au HEAD, mais pas encore dans votre dépôt distant.


Branches overview
*****************

Les branches permettent d'isoler le travaille en cours d'une nouvelle fonctionnalité par rapport au projet
global (branche master). Une fois que le code de la feature est terminé, testé et validé, on doit le
merger/fusionner avec la branche principale (master)

Exemple d'une branche develop qui permet aux dévéloppeur de coder de nouvelles features sans les intégrer directement
à la version production du projet (master). Cela permet de laisser plus de liberté aux dev.


.. figure:: ../_static/git/branches.png
  :align: center
  :width: 300

Merge
*****

Default
=======

.. figure:: ../_static/git/merge.png
  :align: center
  :width: 300

On a une branche feature et on veut la fusionner avec la branche master.
On doit se positionner sur la branch master puis merger la feature.
On procède on utilisants les commandes :

.. code-block:: bash

  $ git checkout master # on se positionne sur master
  $ git merge feature # on fusionne la branche feature sur master

**Inconvénients :**
Tous les commits seront poussé sur master


Squashing
=========

Le squash va permettre d'écraser tous les commits en un seul.

.. figure:: ../_static/git/squashing.png
  :align: center
  :width: 300

.. code-block:: bash

   $ git checkout master
   $ git merge --squash feature

Supprimer commit
****************

.. code-block:: bash

   $ git reset --soft HEAD^1

Rebase
******

.. figure:: ../_static/git/rebase.png
  :align: center
  :width: 300

Nous voudrions transférer notre travail de la branche 'bugFix' directement sur le travail existant dans 'master'.

.. code-block:: bash

   $ git checkout bugFix
   $ git rebase master

.. figure:: ../_static/git/rebase_master.png
  :align: center
  :width: 300

On a bien déplacer le travail de la branche bugFix sur Master. Cependant Master est maintenant en retard.
Il faut alors :

.. code-block:: bash

   $ git checkout master
   $ git rebase bugFix

.. figure:: ../_static/git/rebase_bugFix.png
  :align: center
  :width: 300

Bibliography
************

* [1] Apprendre git ludiquement sur https://learngitbranching.js.org/
* [2] Github, "About merge methods on GitHub" https://help.github.com/articles/about-merge-methods-on-github/
