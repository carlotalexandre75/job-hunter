MongoDB
#######

.. contents::
  :backlinks: top


Connecter mongodb à ElasticSearch
*********************************

On a besoin de :
 * **mongo-connector**
 * **elastic2-doc-manager**

(Il faut biensur tuner certains paramètres)
Mais après cela, chaque insert dans mongoDB sera fait aussi dans ElasticSearch. 😀

Comparaison mongodb vs postgres
*******************************

lire cet `article`_.

.. _article: https://www.sisense.com/fr/blog/postgres-vs-mongodb-for-storing-json-data/

Bibliography
************

* [1] *"Indexing MongoDB with ElasticSearch"*, https://medium.com/@xoor/indexing-mongodb-with-elasticsearch-2c428b676343
