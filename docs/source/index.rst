Notes techniques
################

.. warning::

  Je partage ces notes en l'état. Ce sont des notes personnelles.

  Certaines notes sont fausses. Ce sont des réponses
  que j'ai trouvé sur un problème dans un environnement donné
  avec mon niveau de compétences du moment

  elles sont conçus pour réactiver des "chemins" mentaux
  personnels. elles peuvent donc sembler cryptiques et
  désordonnées à un lecteur tier.


.. toctree::
   :maxdepth: 1
   :numbered:

   git/index
   welcome_to_the_jungle/index
   startup_only/index
   wizbii/index
   postgresql/index
   mongodb/index
   archi/index
