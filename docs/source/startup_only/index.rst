StartUp Only
############


Api access
**********

.. code-block:: bash

   $ curl https://api.startuponly.com/jobs\?keywords\=devops\&superApplicationsSelection\=All\&skip\=0\&take\=10



.. code-block:: json

   {
      "data": [
        {
          "id": "a34b3fe8-9988-4dd8-86b3-a25081fed961",
          "name": "Développeur web Back-end / DevOps",
          "slug": "developpeur-web-back-end-techforgood",
          "city": "Paris",
          "country": "France",
          "street": null,
          "zipcode": "75008",
          "coordinates": {
            "x": 48.871872,
            "y": 2.317643
          },
          "description": "Concrètement, wenabi permet de : \n\n- Sourcer  de nouveaux projets solidaires grâce à un algorithme de matching innovant \n- Faciliter la gestion de la démarche solidaire et la mobilisation des collaborateurs grâce à la digitalisation de nombreux services\n- Suivre et valoriser les actions réalisées grâce à un dashboard automatisé.\n\nEnvironnement technique \n\n# Frontend : AngularJS (migration vers Angular 7), HTML 5, CSS 3, Bootstrap\n# Backend : Java 8, Spring Boot, Hibernate\n# Bases de données : Postgres \n# Autres : JHipster, Webpack, Elastics\n\nTes missions :\n\n- Développement des tests unitaires et d’intégrations (Junit & Mockito)\n- Mise en place de l’intégration continue (Jenkins & Selenium)\n- Automatisation des déploiements sur le Cloud Amazon\n- Evolution possible vers du développement de nouvelles fonctionnalités\n- Etre force de proposition pour améliorer la plateforme",
          "validationStatus": "Validated",
          "visibilityStatus": "Published",
          "contractTypeId": "9edc1ced-c2a6-4e60-9cfa-2e5a2d96b9e8",
          "durationId": "56b38e98-4538-4013-b13b-e0c511aee10a",
          "customDuration": null,
          "company": {
            "id": "001b6117-a0a8-409e-a5cd-f7ae3dc48944",
            "name": "wenabi",
            "slug": "Wenabi-8495",
            "street": null,
            "city": "Paris",
            "zipcode": "75008",
            "country": "France",
            "activities": [
              {
                "id": "f34b0ffd-5722-47c7-9a60-1e17108d53ba",
                "name": "Socialtech",
                "iconName": "fas share-square"
              }
            ],
            "account": {
              "profilePictureUrl": "https://startuponly-v2.s3.eu-west-3.amazonaws.com/e67874e5a8d80d2edf0f27a45a632443.jpg"
            }
          },
          "createDate": "2018-11-13T16:02:34.006Z",
          "updateDate": "2019-02-19T12:04:28.330Z",
          "publishDate": "2018-11-13T16:05:46.236Z",
          "superApplicationEnabled": true,
          "question": "",
          "lookingForDescription": [
            "Futur diplômé d’une école d’ingénieur ou d’informatique – ou autodidacte avec des références sérieuses",
            "Être à l’aise avec la programmation Java",
            "Être curieux, motivé, rigoureux et autonome",
            "Être de bonne humeur et aimer partager des connaissances",
            "La maîtrise d’Angular serait un plus"
          ],
          "skills": [
            {
              "id": "a1b4187c-a991-4fb4-bf51-14ada2b75488",
              "skillLevel": {
                "id": "d9ad0872-7608-4ed7-a78b-6ed74b80586a",
                "description": "undefined",
                "value": 100
              },
              "skillTag": {
                "id": "88acbeb4-ac23-4857-9aad-09eacb052194",
                "slug": "back-end",
                "name": "Back-end"
              }
            },
            {
              "id": "1d922959-9795-4253-a75e-d500d54d9e3a",
              "skillLevel": {
                "id": "d9ad0872-7608-4ed7-a78b-6ed74b80586a",
                "description": "undefined",
                "value": 100
              },
              "skillTag": {
                "id": "7879dd35-1e70-40c3-81c8-ee75d70612fe",
                "slug": "programmation-java",
                "name": "Programmation Java"
              }
            },
            {
              "id": "f2f8a1ad-77ce-4732-b3bd-6993e365f701",
              "skillLevel": {
                "id": "993c4556-4e46-44b8-aa8c-16a97b2f52e3",
                "description": "undefined",
                "value": 66
              },
              "skillTag": {
                "id": "9343b372-4a18-497b-8366-9883431c3e68",
                "slug": "angularjs-en-formation",
                "name": "AngularJS (en formation)"
              }
            },
            {
              "id": "4886cd5a-992f-4bc2-846b-4cffb9e40b30",
              "skillLevel": {
                "id": "993c4556-4e46-44b8-aa8c-16a97b2f52e3",
                "description": "undefined",
                "value": 66
              },
              "skillTag": {
                "id": "b094b703-67b1-4ce6-8447-e5cb713967fd",
                "slug": "tests-utilisateurs",
                "name": "Tests utilisateurs"
              }
            }
          ],
          "plus": [],
          "startAtFirstDate": "2019-01-06T23:00:00.000Z",
          "startAtLeastDate": "2019-04-07T22:00:00.000Z",
          "applicationsCount": 0,
          "newApplicationsCount": 0,
          "newSuperApplicationsCount": 0,
          "seenApplicationsCount": 0,
          "shortlistedApplicationsCount": 0,
          "superApplicationsCount": 0,
          "isOneShot": false,
          "lastIndexedDate": "2018-12-03T11:00:08.073Z",
          "nextReindexDate": "2018-12-18T11:00:08.073Z",
          "unpublishDate": null,
          "category": {
            "id": "7fa3b587-0146-43cd-9b58-3d59edcd4228",
            "name": "Tech"
          },
          "canBeReIndexed": false,
          "maxSuperApplicationReached": false
        }
      ],
      "total": 1
   }
