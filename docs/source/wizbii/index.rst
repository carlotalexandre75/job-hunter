Wizbii
#####################

Api access
**********

.. code-block:: bash

    $ curl 'https://ug9ksbmo1v-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20vanilla%20JavaScript%203.31.0&x-algolia-application-id=UG9KSBMO1V&x-algolia-api-key=5c1caf750df762e6f99c11067353408e' \
    -H 'accept: application/json' \
    -H 'Referer: https://www.wizbii.com/search/jobs?q=devops' \
    -H 'Origin:  https://www.wizbii.com' \
    -H 'content-type: application/x-www-form-urlencoded' \
    --data '{"requests":[{"indexName":"prod_jobs","params":"query=devops&queryLanguages=%5B%22fr%22%5D&removeStopWords=true&ignorePlurals=true&hitsPerPage=0&facets=%5B%22contract.titleShort%22%5D&facetFilters=%5B%5B%5D%2C%5B%5D%2C%5B%5D%5D"},{"indexName":"prod_jobs","params":"query=devops&queryLanguages=%5B%22fr%22%5D&removeStopWords=true&ignorePlurals=true&hitsPerPage=0&facets=%5B%22domain.id%22%5D&facetFilters=%5B%5B%5D%2C%5B%5D%2C%5B%5D%5D"},{"indexName":"prod_jobs","params":"query=devops&queryLanguages=%5B%22fr%22%5D&removeStopWords=true&ignorePlurals=true&page=0&hitsPerPage=20&attributesToRetrieve=%5B%22*%22%5D&facetFilters=%5B%5B%5D%2C%5B%5D%2C%5B%5D%2C%5B%5D%5D"}]}' \
    --compressed | jq '.'